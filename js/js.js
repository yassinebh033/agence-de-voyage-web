$(document).ready(function () {


        

    
    var getUrlParameter = function getUrlParameter(sParam) {
        var sPageURL = window.location.search.substring(1),
         sURLVariables = sPageURL.split('&'),
         sParameterName,
         i;
        
        for (i = 0; i < sURLVariables.length; i++) {
         sParameterName = sURLVariables[i].split('=');
        
         if (sParameterName[0] === sParam) {
             return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
         }
        }
        };
        
       let id = getUrlParameter('id');
       

       $.ajax({
        cache: false,
        type: 'GET',
        url: 'https://freedomtravel.tn/json/ville.php',
        success: function (villes) {
            villes = $.parseJSON(villes);
            console.log(villes);
    
            $.each(villes, function (index, data) {
                $('#villehome').append('<option value="' + data.id + '">' + data.nom + '</option>');
                $('#selectville').append('<option value="' + data.id + '">' + data.nom + '</option>');
            });
        }
    });
    


    $.ajax({
        cache: false,
        type: 'GET',
        url: 'https://www.freedomtravel.tn/json/hotels.php',
    
        success: function (hotels) {
            hotels = $.parseJSON(hotels);
            console.log(hotels);
    
            $.each(hotels, function (index, data) {
    
                if (data.categorie=="2") {
                    $('#card').append('<div class="col-md-4 "><div class="destination"><a href="#" class="img img-2 d-flex justify-content-center align-items-center" style="background-image: url(https://www.freedomtravel.tn/assets/images/hotel_miniature/'+data.id+'.jpg );"></a><div class="text p-3"><div class="d-flex"><div class="one"><h3><a href="#">'+data.nom+'</a></h3><p class="rate" id="icone"><span class="star"><i class="icon-star"></i><i class="icon-star"></i><span>'+data.categorie+' Rating </span></span></p></div><div class="two"><span class="price per-price">'+data.lpdvente+'<small>LPD/night</small></span><br/><span class="price per-price">'+data.dpvente+'<small>DP/night</small></span></div></div><p><span><i class="icon-map-o"></i> '+data.ville+'</span> </p><hr> <span class="ml-auto text-right"><a href="hotel-single.html?id=' + data.id + '"> <button type="button" class="btn btn-primary" id="reservation">Book Now</button> </a></span></div></div></div>');
                }
                if  (data.categorie=="3") {
                    $('#card').append('<div class="col-md-4 "><div class="destination"><a href="#" class="img img-2 d-flex justify-content-center align-items-center" style="background-image: url(https://www.freedomtravel.tn/assets/images/hotel_miniature/'+data.id+'.jpg );"></a><div class="text p-3"><div class="d-flex"><div class="one"><h3><a href="#">'+data.nom+'</a></h3><p class="rate" id="icone"><span class="star"><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><span> '+data.categorie+' Rating</span></span></p></div><div class="two"><span class="price per-price">'+data.lpdvente+'<small>LPD/night</small></span><br/><span class="price per-price">'+data.dpvente+'<small>DP/night</small></div></div><p><span><i class="icon-map-o"></i> '+data.ville+'</span> </p><hr> <span class="ml-auto text-right"><a href="hotel-single.html?id=' + data.id + '"> <button type="button" class="btn btn-primary" id="reservation">Book Now</button> </a></span></div></div></div>');
                }
                if  (data.categorie=="4") {
                    $('#card').append('<div class="col-md-4 "><div class="destination"><a href="#" class="img img-2 d-flex justify-content-center align-items-center" style="background-image: url(https://www.freedomtravel.tn/assets/images/hotel_miniature/'+data.id+'.jpg );"></a><div class="text p-3"><div class="d-flex"><div class="one"><h3><a href="#">'+data.nom+'</a></h3><p class="rate" id="icone"><span class="star"><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><span> '+data.categorie+' Rating</span></span></p></div><div class="two"><span class="price per-price">'+data.lpdvente+'<small>LPD/night</small></span><br/><span class="price per-price">'+data.dpvente+'<small>DP/night</small></div></div><p><span><i class="icon-map-o"></i> '+data.ville+'</span> </p><hr> <span class="ml-auto text-right"><a href="hotel-single.html?id=' + data.id + '"> <button type="button" class="btn btn-primary" id="reservation">Book Now</button> </a></span></div></div></div>');
    
                }
                if  (data.categorie=="5") {
                    $('#card').append('<div class="col-md-4 "><div class="destination"><a href="#" class="img img-2 d-flex justify-content-center align-items-center" style="background-image: url(https://www.freedomtravel.tn/assets/images/hotel_miniature/'+data.id+'.jpg );"></a><div class="text p-3"><div class="d-flex"><div class="one"><h3><a href="#">'+data.nom+'</a></h3><p class="rate" id="icone"><span class="star"><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><span> '+data.categorie+' Rating</span></span></p></div><div class="two"><span class="price per-price">'+data.lpdvente+'<small>LPD/night</small></span><br/><span class="price per-price">'+data.dpvente+'<small>DP/night</small></div></div><p><span><i class="icon-map-o"></i> '+data.ville+'</span> </p><hr> <span class="ml-auto text-right"><a href="hotel-single.html?id=' + data.id + '"> <button type="button" class="btn btn-primary" id="reservation">Book Now</button> </a></span></div></div></div>');
                
                }
        if (data.id==id && data.categorie=="2") {
            $('#detailhotel').append('<span>Our Best hotels &amp; Rooms</span><h2>Luxury Hotel in '+data.ville+'</h2><p class="rate mb-5"><span class="loc"><a href="#"><i class="icon-map"></i> '+data.nom+' </a></span><span class="star" id="2"><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star-o"></i><i class="icon-star-o"></i><i class="icon-star-o"></i>'+data.categorie+' Rating</span></p><p>Faites-vous chouchouter comme une star en profitant du service exclusif de létablissement '+data.nom+' <br/><strong>Ses points forts:</strong></p><div class="d-md-flex mt-5 mb-5"><ul><li><i class="fas fa-wifi">  Connexion Wi-Fi gratuite</i></li><li><i class="fas fa-umbrella-beach">  Plage privée</i></li><li><i class="fas fa-parking">  Parking gratuit</i>  </li><li> <i class="far fa-star">  Des suites familiales</i></li></ul><ul class="ml-md-5"><li><i class="fas fa-spa">  Spa et centre de bien-être</i></li><li><i class="fas fa-dumbbell">  Fitness center/gym</i> </li><li><i class="fas fa-swimmer">  Piscine exterieure et  Piscine intérieure</i></li><li><i class="fas fa-glass-martini-alt"> Bar</i></li></ul></div>');
            $('#titrehotel').append('<h3 class="mb-3 bread">'+data.nom+'</h3>');
        }
        if (data.id==id && data.categorie=="3") {
            $('#detailhotel').append('<span>Our Best hotels &amp; Rooms</span><h2>Luxury Hotel in '+data.ville+'</h2><p class="rate mb-5"><span class="loc"><a href="#"><i class="icon-map"></i> '+data.nom+' </a></span><span class="star" id="3"><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star-o"></i><i class="icon-star-o"></i>'+data.categorie+' Rating</span></p><p>Faites-vous chouchouter comme une star en profitant du service exclusif de létablissement '+data.nom+' <br/><strong>Ses points forts:</strong></p><div class="d-md-flex mt-5 mb-5"><ul><li><i class="fas fa-wifi">  Connexion Wi-Fi gratuite</i></li><li><i class="fas fa-umbrella-beach">  Plage privée</i></li><li><i class="fas fa-parking">  Parking gratuit</i>  </li><li> <i class="far fa-star">  Des suites familiales</i></li></ul><ul class="ml-md-5"><li><i class="fas fa-spa">  Spa et centre de bien-être</i></li><li><i class="fas fa-dumbbell">  Fitness center/gym</i> </li><li><i class="fas fa-swimmer">  Piscine exterieure et  Piscine intérieure</i></li><li><i class="fas fa-glass-martini-alt"> Bar</i></li></ul></div>');
            $('#titrehotel').append('<h3 class="mb-3 bread">'+data.nom+'</h3>');

        }
        if (data.id==id && data.categorie=="4") {
            $('#detailhotel').append('<span>Our Best hotels &amp; Rooms</span><h2>Luxury Hotel in '+data.ville+'</h2><p class="rate mb-5"><span class="loc"><a href="#"><i class="icon-map"></i> '+data.nom+' </a></span><span class="star" id="4"><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star-o"></i>'+data.categorie+' Rating</span></p><p>Faites-vous chouchouter comme une star en profitant du service exclusif de létablissement '+data.nom+' <br/><strong>Ses points forts:</strong></p><div class="d-md-flex mt-5 mb-5"><ul><li><i class="fas fa-wifi">  Connexion Wi-Fi gratuite</i></li><li><i class="fas fa-umbrella-beach">  Plage privée</i></li><li><i class="fas fa-parking">  Parking gratuit</i>  </li><li> <i class="far fa-star">  Des suites familiales</i></li></ul><ul class="ml-md-5"><li><i class="fas fa-spa">  Spa et centre de bien-être</i></li><li><i class="fas fa-dumbbell">  Fitness center/gym</i> </li><li><i class="fas fa-swimmer">  Piscine exterieure et  Piscine intérieure</i></li><li><i class="fas fa-glass-martini-alt"> Bar</i></li></ul></div>');
            $('#titrehotel').append('<h3 class="mb-3 bread">'+data.nom+'</h3>');

        }
        if (data.id==id && data.categorie=="5") {
            $('#detailhotel').append('<span>Our Best hotels &amp; Rooms</span><h2>Luxury Hotel in '+data.ville+'</h2><p class="rate mb-5"><span class="loc"><a href="#"><i class="icon-map"></i> '+data.nom+' </a></span><span class="star" id="5"><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i>'+data.categorie+' Rating</span></p><p>Faites-vous chouchouter comme une star en profitant du service exclusif de létablissement '+data.nom+' <br/><strong>Ses points forts:</strong></p><div class="d-md-flex mt-5 mb-5"><ul><li><i class="fas fa-wifi">  Connexion Wi-Fi gratuite</i></li><li><i class="fas fa-umbrella-beach">  Plage privée</i></li><li><i class="fas fa-parking">  Parking gratuit</i>  </li><li> <i class="far fa-star">  Des suites familiales</i></li></ul><ul class="ml-md-5"><li><i class="fas fa-spa">  Spa et centre de bien-être</i></li><li><i class="fas fa-dumbbell">  Fitness center/gym</i> </li><li><i class="fas fa-swimmer">  Piscine exterieure et  Piscine intérieure</i></li><li><i class="fas fa-glass-martini-alt"> Bar</i></li></ul></div>');
            $('#titrehotel').append('<h3 class="mb-3 bread">'+data.nom+'</h3>');

        }

            });
    
                
        }
    });
    




    $.ajax({
        cache: false,
        type: 'GET',
        url: 'https://www.freedomtravel.tn/json/voyages.php',
    
        success: function (voyages) {
            voyages = $.parseJSON(voyages);
            console.log(voyages);
    
            $.each(voyages, function (index, data) {
               // $('#carouvoyhome').append('<div class="item"><div class="destination"><a href="voyage.html" class="img d-flex justify-content-center align-items-center" style="background-image: url(images/destination-4.jpg);"><div class="icon d-flex justify-content-center align-items-center"><span class="icon-search2"></span></div></a><div class="text p-3"><h3><a href="voyage.html">'+data.titre+'</a></h3><span class="listing">A partir<ins>'+data.prix+'</ins>DT</span><span class="price night"><ins>  '+data.nbrejour+' jours / '+data.nbrenuitee+' nuitées</ins></span></div></div></div>');

                    $('#cardvoyage').append('<!-- Card --> <div class="card card-cascade wider reverse" id="'+data.titre+'"><!-- Card image --><div class="view view-cascade overlay"><img class="card-img-top" src="https://www.freedomtravel.tn/assets/images/4/'+data.id+'/'+data.nom+'" alt="Card image cap"><a href="#"><div class="mask rgba-white-slight"></div> </a></div><!-- Card content --><div class="card-body card-body-cascade text-center"><!-- Title --><h3 class="card-title text-center"><strong>'+data.titre+'</strong></h3> <!-- Subtitle --><h5 class="font-weight-bold indigo-text py-2 text-center"><ins> Loffre Comprend : </ins></h5><!-- Text --><div class="row "><div class="col-md-6 col-sm-6 text-left "><p class="card-text "><strong><i class="fas fa-plane-departure"></i> Billet davion Aller/Retour. <br /><i class="fas fa-hotel"></i> Sejour Hotel en LPD. <br /><i class="fas fa-skiing"></i> Excurtions <br /><i class="fas fa-route"></i> Transfert Aeropot / Hôtel</strong></p></div><div class="col-md-3 col-sm-3 "><span class="price night"><ins><strong>  '+data.nbrejour+' jours / '+data.nbrenuitee+' nuitées</strong></ins></span></div><div class="col-md-3 col-sm-3 "><div class="price-box"><h6 class="price"><strong> A partir<ins>'+data.prix+'</ins>DT</strong></h6></div></div></div><div class="card-footer card-footer-cascade text-right "><button action="#" class="btn blue-gradient">Réservation</button></div></div></div><!-- Card --><br/><br/>');
                    $('#selectvoyage').append(' <option value="'+data.titre+'">'+data.titre+'</option>');
                });    			

    
                $('#selectvoyage').change(function (event) {

                    event.preventDefault();

                  var  selectvoyage1= $("#selectvoyage").val().toLowerCase();

                    console.log(selectvoyage1);
                 
                       
                    
                      

                });

            }
        });
        


        let t=0;

let activ = "active";
$.ajax({
	cache: false,
	type: 'GET',
	url: 'https://www.freedomtravel.tn/json/carousel_home_page.php',
	success: function (data) {
		data = $.parseJSON(data);
		console.log(data);
		$.each(data, function (index, d) {
            
        
			$('#carouHome').append(' <div class="carousel-item '+activ+'"><div class="view"><img class="d-block w-100" src="'+data[t].src+'"alt="'+data[t].id+'"> <div class="mask rgba-black-light"></div></div></div>');
            activ = "";
            console.log(data[t].src);

            t++;
        
		});
	}
});









  
let active = "active";
$.ajax({
	cache: false,
	type: 'GET',
	url: 'https://www.freedomtravel.tn/json/carouselHotel.php',
	data: { id: id },
	success: function (data) {
		data = $.parseJSON(data);
		console.log(data);
		console.log(id);
		$.each(data, function (index, d) {

			$('#carousel').append('<div class="carousel-item '+active+'"><img class="d-block w-100" src="https://www.freedomtravel.tn/' + d.href + '" alt="First slide"></div>');
            active = "";
            
		});
	}
});


let datein;
let dateout;

let tarif;
   let chambre;
    let d;
    let arrangement;
   let  j=0;

$(function() {

    $('input[name="datefilter"]').daterangepicker({
        autoUpdateInput: false,
        locale: {
            cancelLabel: 'Clear'
        }
    });
  
    $('input[name="datefilter"]').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('DD-MM-YYYY') + ' to ' + picker.endDate.format('DD-MM-YYYY'));
         datein=picker.startDate.format('YYYY-MM-DD');
         dateout=picker.endDate.format('YYYY-MM-DD');
        console.log(datein, dateout);
       
    
            $.ajax({
                cache: false,
                type: 'POST',
                url: ' https://www.freedomtravel.tn/ng/disponibilitehotel.php',
                data: { id: id, in: datein, out: dateout },
                success: function (dispo) {
                    dispo = $.parseJSON(dispo);
                    console.log(dispo);
                    console.log(datein, dateout);
    
                }
            });
       

                $.ajax({
                    cache: false,
                    type: 'GET',
                    url: 'https://www.freedomtravel.tn/json/grilleTarifaireHotel.php',
                    data: { id: id, check_in: datein, check_out: dateout },
                    success: function (tarif) {
                        tarif = $.parseJSON(tarif);
                        chambre = tarif.chambre;
                        arrangement = tarif.arrangement;
                        
                        console.log(arrangement);
                        console.log(chambre);
                       $.each(arrangement,function(index,d) {
                        $('#inputarrangement'+j).append('<option value="'+d+'">'+d+'</option>');
                         
                   
                    });
                    
                       
                    }
                    
                });

            
        });
        
  
    $('input[name="datefilter"]').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
    });
  
  });
  




    $('#newsletterform').submit(function (event) {

        event.preventDefault();
        email = $(this).find("#inputmail").val();
        
    
        $.post('https://www.freedomtravel.tn/json/addnewsLetterForm.php', { email: email },
            function () {
    
                $('#inputmail').replaceWith('<input type="text" class="form-control" placeholder="@email envoyer avec succés" name=email id="inputmail">');
            });
        return false ;
    });

  
    $('#forminscri').submit(function (event) {

        event.preventDefault();
        name= $(this).find("input[name=name]").val();
        email = $(this).find("input[name=email]").val();
        tel=$(this).find("input[name=tel]").val();
        sujet=$(this).find("input[name=sujet]").val();
        message=$(this).find("textarea[name=message]").val();
        $.post('https://www.freedomtravel.tn/json/addcontactform.php', { name: name, email: email , tel: tel , sujet: sujet , message: message },
            function (result) {
    console.log("form submited");
                $('#forminscri').replaceWith('<h3>Votre message a été envoyer avec succes..MERCI!<i class="far fa-smile-beam"></i></h3>');
            });
        return false ;
    });


    $('#form_omra').submit(function (event) {

        event.preventDefault();
        nom= $(this).find("#name").val();
        tel=$(this).find("#tel").val();
        email = $(this).find("#email").val();
        dateDep=$(this).find("#datedep").val();
        typeHeberg=$(this).find("#typeheberg").val();
        nbre_participant=$(this).find("#nbr_parti").val();
        console.log(nom,tel,email,dateDep,typeHeberg,nbre_participant);
        $.post('https://www.freedomtravel.tn/ng/preinscription_omra.php', { nom: nom, email: email , tel: tel , dateDep: dateDep , typeHeberg: typeHeberg , nbre_participant : nbre_participant },
            function (result) {
    console.log("form omra submited:",nom,tel,email,dateDep,typeHeberg,nbre_participant);
                $('#form_omra').replaceWith('<div class="form-group text-right col-md-8"> <h3>  <i class="far fa-smile-beam"></i> تم تسجيل معطياتكم بنجاح..شكرا على ثقتكم   <i class="far fa-address-card"></i> </h3></div>');
            });
        return false ;
    });

    
    
    
   
        
   

   // for (j=0;j<=i;j++){
      
        $('#inputadulte'+j ||'#inputenfant'+j).change(function () {
           
            
            if (($('#inputadulte'+j ).val()) == 1 && ($('#inputenfant'+j).val()) == 0 ) {
                 console.log(chambre[0]);
                 $('#inputarrangement'+j ).change(function(){
                    if ( $('#inputarrangement'+j ).val() == "LPD") {
                       
                        $('#price'+j+'').text(chambre[0].LPD);
                        
                    }
                else if ( $('#inputarrangement'+j+'').val() == "DP") {
                        
                        $('#price'+j+'').text(chambre[0].DP);
                 }
                  else if ($('#inputarrangement'+j+'').val() == "PC"){
                            
                            $('#price'+j+'').text(chambre[0].PC);
                  }
                    });
             
            }
              
       else if (($('#inputadulte'+j).val())==0 && ($('#inputenfant'+j).val())==1) {
               
               console.log(chambre[1]);
               $('#inputarrangement'+j).change(function(){
                if ( $('#inputarrangement'+j).val() == "LPD") {
                   
                    $('#price'+j).text(chambre[1].LPD);
                    
                }
            else if ( $('#inputarrangement'+j).val() == "DP") {
                    
                    $('#price'+j).text(chambre[1].DP);
             }
              else if ($('#inputarrangement'+j).val() == "PC"){
                        
                        $('#price'+j).text(chambre[1].PC);
              }
                });
           }
           else if (($('#inputadulte'+j).val())==2 && ($('#inputenfant'+j).val())==0) {
            
           console.log(chambre[2]);
           $('#inputarrangement'+j).change(function(){
            if ( $('#inputarrangement'+j).val() == "LPD") {
               
                $('#price'+j).text(chambre[2].LPD);
                
            }
        else if ( $('#inputarrangement'+j).val() == "DP") {
                
                $('#price'+j).text(chambre[2].DP);
         }
          else if ($('#inputarrangement'+j).val() == "PC"){
                    
                    $('#price'+j).text(chambre[2].PC);
          }
            });
           }
           else if (($('#inputadulte'+j).val())==0 && ($('#inputenfant'+j).val())==1) {
           
           console.log(chambre[3]);
           $('#inputarrangement'+j).change(function(){
            if ( $('#inputarrangement'+j).val() == "LPD") {
               
                $('#price'+j).text(chambre[3].LPD);
                
            }
        else if ( $('#inputarrangement'+j).val() == "DP") {
                
                $('#price'+j).text(chambre[3].DP);
         }
          else if ($('#inputarrangement'+j).val() == "PC"){
                    
                    $('#price'+j).text(chambre[3].PC);
          }
            });
           }
          else if (($('#inputadulte'+j).val())==0 && ($('#inputenfant'+j).val())==2) {
          console.log(chambre[4]);
          $('#inputarrangement'+j).change(function(){
            if ( $('#inputarrangement'+j).val() == "LPD") {
               
                $('#price'+j).text(chambre[4].LPD);
                
            }
        else if ( $('#inputarrangement'+j).val() == "DP") {
                
                $('#price'+j).text(chambre[4].DP);
         }
          else if ($('#inputarrangement'+j).val() == "PC"){
                    
                    $('#price'+j).text(chambre[4].PC);
          }
            });
          }
          else if (($('#inputadulte'+j).val())==3 && ($('#inputenfant'+j).val())==0) {
         
             console.log(chambre[5]);
             $('#inputarrangement'+j).change(function(){
                if ( $('#inputarrangement'+j).val() == "LPD") {
                   
                    $('#price'+j).text(chambre[5].LPD);
                    
                }
            else if ( $('#inputarrangement'+j).val() == "DP") {
                    
                    $('#price'+j).text(chambre[5].DP);
             }
              else if ($('#inputarrangement'+j).val() == "PC"){
                        
                        $('#price'+j).text(chambre[5].PC);
              }
                });
           }
           else if ($('#inputadulte'+j).val() == 2 && $('#inputenfant'+j).val() == 1) {
         
           console.log(chambre[6]);
           $('#inputarrangement'+j).change(function(){
            if ( $('#inputarrangement'+j).val() == "LPD") {
               
                $('#price'+j).text(chambre[6].LPD);
                
            }
        else if ( $('#inputarrangement'+j).val() == "DP") {
                
                $('#price'+j).text(chambre[6].DP);
         }
          else if ($('#inputarrangement'+j).val() == "PC"){
                    
                    $('#price'+j).text(chambre[6].PC);
          }
            });
            }
          else if (($('#inputadulte'+j).val())==1 && ($('#inputenfant'+j).val())==2) {
           
          console.log(chambre[7]);
          $('#inputarrangement'+j).change(function(){
            if ( $('#inputarrangement'+j).val() == "LPD") {
               
                $('#price'+j).text(chambre[7].LPD);
                
            }
        else if ( $('#inputarrangement'+j).val() == "DP") {
                
                $('#price'+j).text(chambre[7].DP);
         }
          else if ($('#inputarrangement'+j).val() == "PC"){
                    
                    $('#price'+j).text(chambre[7].PC);
          }
            });
           }
           else if (($('#inputadulte'+j).val())=='0' && ($('#inputenfant'+j).val())==3) {
          
            console.log(chambre[8]);
            $('#inputarrangement'+j).change(function(){
                if ( $('#inputarrangement'+j).val() == "LPD") {
                   
                    $('#price'+j).text(chambre[8].LPD);
                    
                }
            else if ( $('#inputarrangement'+j).val() == "DP") {
                    
                    $('#price'+j).text(chambre[8].DP);
             }
              else if ($('#inputarrangement'+j).val() == "PC"){
                        
                        $('#price'+j).text(chambre[8].PC);
              }
                });
             }
        else if (($('#inputadulte'+j).val())==4 && ($('#inputenfant'+j).val())==0) {
         
            console.log(chambre[9]);
            $('#inputarrangement'+j).change(function(){
                if ( $('#inputarrangement'+j).val() == "LPD") {
                   
                    $('#price'+j).text(chambre[9].LPD);
                    
                }
            else if ( $('#inputarrangement'+j).val() == "DP") {
                    
                    $('#price'+j).text(chambre[9].DP);
             }
              else if ($('#inputarrangement'+j).val() == "PC"){
                        
                        $('#price'+j).text(chambre[9].PC);
              }
                });
          }
        else if (($('#inputadulte'+j).val())==3 && ($('#inputenfant'+j).val())==1) {
        
         console.log(chambre[10]);
         $('#inputarrangement'+j).change(function(){
            if ( $('#inputarrangement'+j).val() == "LPD") {
               
                $('#price'+j).text(chambre[10].LPD);
                
            }
        else if ( $('#inputarrangement'+j).val() == "DP") {
                
                $('#price'+j).text(chambre[10].DP);
         }
          else if ($('#inputarrangement'+j+'').val() == "PC"){
                    
                    $('#price'+j).text(chambre[10].PC);
          }
            });
          }
         else if (($('#inputadulte'+j).val())==2 && ($('#inputenfant'+j).val())==2) {
         
          console.log(chambre[11]);
          $('#inputarrangement'+j).change(function(){
            if ( $('#inputarrangement'+j).val() == "LPD") {
               
                $('#price'+j).text(chambre[11].LPD);
                
            }
        else if ( $('#inputarrangement'+j).val() == "DP") {
                
                $('#price'+j).text(chambre[11].DP);
         }
          else if ($('#inputarrangement'+j).val() == "PC"){
                    
                    $('#price'+j).text(chambre[11].PC);
          }
            });
            }
         else if (($('#inputadulte'+j).val())==1 && ($('#inputenfant'+j).val())==3) {
            
         console.log(chambre[12]);
         $('#inputarrangement'+j).change(function(){
            if ( $('#inputarrangement'+j).val() == "LPD") {
               
                $('#price'+j+'').text(chambre[12].LPD);
                
            }
        else if ( $('#inputarrangement'+j).val() == "DP") {
                
                $('#price'+j).text(chambre[12].DP);
         }
          else if ($('#inputarrangement'+j).val() == "PC"){
                    
                    $('#price'+j).text(chambre[12].PC);
          }
            });
           }
             else if (($('#inputadulte'+j).val())==0 && ($('#inputenfant'+j).val())==4){
         
          console.log(chambre[13]);
          $('#inputarrangement'+j).change(function(){
            if ( $('#inputarrangement'+j).val() == "LPD") {
               
                $('#price'+j).text(chambre[13].LPD);
                
            }
        else if ( $('#inputarrangement'+j).val() == "DP") {
                
                $('#price'+j).text(chambre[13].DP);
         }
          else if ($('#inputarrangement'+j).val() == "PC"){
                    
                    $('#price'+j).text(chambre[13].PC);
          }
            });
            }
            
            else {
                console.log("false") ;
                }    
            
         }); 
    
       // }


        var i=1;
  
        $('#chambre').click(function () {
            
            
            $("#pra").append('<div class="form-group col-md-3"><label for="inputadu">adulte(s)</label><select id="inputadulte'+i+'" class="form-control"><option value="0" selected>0</option><option value="1">1</option><option value="2">2</option><option value="3">3</option ><option value="4">4</option></select></div><div class="form-group col-md-3"><label for="inputenf">enfant(s)</label><select id="inputenfant'+i+'" class="form-control"><option value="0" selected>0</option><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option></select></div><div class="form-group col-md-3"><label for="inputarr">Arrangement</label><select id="inputarrangement'+i+'" class="form-control"><option selected>choose</option></select></div><div class="form-group col-md-3" id="price'+i+'"><label for="from">Prix</label></div>');
           
            $.each(arrangement,function(index,d) {
                $('#inputarrangement'+i).append('<option value="'+d+'">'+d+'</option>');
                 
                i++;
 
            });
            
            return false;
           
    });
   
        
    $('#formreserv').submit(function (event) {
var client = new Object();
var vente_hotel= new Object();

        event.preventDefault();
        nom= $('#inputname').val();
      mail =$('#inputemail').val();
      tel=$('#inputtel').val();
        type_chambre= [$('#inputadulte'+j).val()];
        f_nbre_bb= [$(this).find($("#inputenfant"+j)).val()];
        f_arrangement=[$('#inputarrangement'+j).val()];
        client.nom= nom;
        client.mail=mail;
        client.tel=tel;
        vente_hotel.entree=datein;
        vente_hotel.sortie=dateout;
        vente_hotel.hotel=parseInt(id);
        $.post('https://www.freedomtravel.tn/json/ajouter_sejour_hotel.php', {  client : client, vente_hotel : vente_hotel },
           function (result) {
       console.log("mrigl", client);
       console.log(result);

               $('#formreserv').replaceWith('<h3> votre reservation a été envoyer avec succes. vous resevrez le voucher sur votre email.MERCI!! </h3>');
            });
        return false ;
    });



        


     



      

      


    });
